/**
 * AuthController
 * @description :: Server-side logic for managing user's authorization
 */
const passport = require("passport");
const moment = require("moment");
/**
 * Triggers when user authenticates via passport
 * @param {Object} req Request object
 * @param {Object} res Response object
 * @param {Object} error Error object
 * @param {Object} user User profile
 * @param {Object} info Info if some error occurs
 * @private
 */

module.exports = {
  /**
   * Sign up in system
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  signup: function(req, res) {
    req.body.email = req.body.email.trim().toLowerCase();
    req.body.role = req.body.role; // TODO: Allow for sending array of roles to add to user.

    // PASSWORD REQUIREMENTS:
    // Minimum length: 8
    // At least one lowercase letter
    // At least one uppercase letter
    // At least one number
    // At least one special character

    if (!req.body.password || !req.body.role) {
      return res.badRequest();
    }

    const passwordRegex = /^(?=.*\d)(?=.*[!@#$%^&*_.()])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    if (req.body.password && !passwordRegex.test(req.body.password)) {
      // According to Sails docs, message will not display in Production mode.
      // Sails v1.0 has enhancements to badRequest behavior.
      return res.badRequest(
        "Password must be at least 8 characters, with one lowercase letter, one uppercase letter, one number, and a special character."
      );
    }

    return Role.findOne({
      where: {
        name: req.body.role
      }
    })
      .then(role => {
        if (!role) {
          return res.serverError(`No role named: ${req.body.role} existed.`);
        }
        return User.create(_.omit(req.allParams(), "id"))
          .then(user => {
            return UserRole.create({ roleId: role.id, userId: user.id }).then(
              userRole => {
                return {
                  token: CipherService.createToken(user),
                  user: user
                };
              }
            );
          })
          .then(() => {
            return res.ok();
          })
          .catch(err => {
            return res.serverError(err);
          });
      })
      .catch(err => {
        return res.serverError(err);
      });
  },

  createSuperAdmin: function(req, res) {
    req.body.email = req.body.email.trim().toLowerCase();

    if (req.body.roles && _.includes(req.body.roles, "user")) {
      return res.forbidden();
    }
    if (
      !req.body.roles ||
      (req.body.roles && !_.includes(req.body.roles, "admin"))
    ) {
      return res.forbidden();
    }

    if (
      process.env.CREATE_ADMIN_PASSWORD &&
      req.headers &&
      req.headers.authorization === process.env.CREATE_ADMIN_PASSWORD
    ) {
      return Role.findOne({
        where: {
          name: sails.config.jwtSettings.superAdminRole
        }
      })
        .then(role => {
          if (!role) {
            return res.serverError(
              `No role named: ${
                sails.config.jwtSettings.superAdminRole
              } existed.`
            );
          }
          return User.create(_.omit(req.allParams(), "id"))
            .then(user => {
              return UserRole.create({ roleId: role.id, userId: user.id }).then(
                userRole => {
                  return {
                    token: CipherService.createToken(user),
                    user: user
                  };
                }
              );
            })
            .then(() => {
              return res.ok();
            })
            .catch(err => {
              return res.serverError(err);
            });
        })
        .catch(err => {
          return res.serverError(err);
        });
    }
    return res.forbidden();
  },

  updatePassword: (req, res) => {
    if (!req.body.password) {
      return res.badRequest();
    }
    if (!req.body.email) {
      return res.badRequest();
    }
    if (!req.user) {
      return res.forbidden();
    }
    // PASSWORD REQUIREMENTS:
    // Minimum length: 8
    // At least one lowercase letter
    // At least one uppercase letter
    // At least one number
    // At least one special character
    const passwordRegex = /^(?=.*\d)(?=.*[!@#$%^&*_.()])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    if (!passwordRegex.test(req.body.password)) {
      // According to Sails docs, message will not display in Production mode.
      // Sails v1.0 has enhancements to badRequest behavior.
      return res.badRequest(
        "Password must be at least 8 characters, with one lowercase letter, one uppercase letter, one number, and a special character."
      );
    }
    const newPassword = CipherService.hashPassword(req.body.password);
    return User.update(
      { password: newPassword },
      { where: { email: req.body.email } }
    ).then(() => {
      return res.ok();
    });
  },

  /**
   * Sign in by local strategy in passport
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  login: function(req, res) {
    if (!req.body.email || !req.body.password) {
      return res.badRequest();
    }
    req.body.email = req.body.email.trim().toLowerCase();
    UserService.attemptSignIn(req, res);
  },

  signout: function(req, res) {
    if (req.user && req.user.email) {
      const user = req.user;
      return Token.update(
        { revoke: true },
        { where: { user: user.id, revoke: false } }
      )
        .then(() => {
          return User.update(
            { isLoggedIn: false },
            { where: { email: user.email } }
          )
            .then(() => {
              return res.ok();
            })
            .catch(err => {
              return res.serverError(err);
            });
        })
        .catch(err => {
          return res.serverError(err);
        });
    }
    return res.badRequest();
  },

  subscribeAll: function(req, res) {
    if (!req.isSocket) {
      console.log("Bad Request occurred while subscribing: ");
      return res.badRequest();
    }

    const joinRoom = (req, roomName) => {
      return new Promise((resolve, reject) => {
        return sails.sockets.join(req, roomName, function(err) {
          if (err) {
            return reject(err);
          }
          return resolve();
        });
      });
    };
    const userId = req.query.userId || "";
    const roomArray = [];
    roomArray.push(joinRoom(req, "notificationRoom"));

    return Promise.all(roomArray)
      .then(() => {
        return res.ok("subscribed");
      })
      .catch(err => {
        console.log("Error occurred while subscribing: ");
        console.log(err);
        return res.serverError(err);
      });
  },

  unsubscribeAll: function(req, res) {
    if (!req.isSocket) {
      return res.badRequest();
    }

    const leaveRoom = (req, roomName) => {
      return new Promise((resolve, reject) => {
        return sails.sockets.leave(req, roomName, function(err) {
          if (err) {
            return reject(err);
          }
          return resolve();
        });
      });
    };

    const roomArray = [];
    const userId = req.param("userId") ? req.param("userId") : "";
    roomArray.push(leaveRoom(req, "notificationRoom"));

    return Promise.all(roomArray)
      .then(() => {
        return res.ok("unsubscribed");
      })
      .catch(err => {
        console.log("Error occurred while subscribing: ");
        console.log(err);
        return res.serverError(err);
      });
  },

  error: function(req, res) {
    myUndefinedFunction();
    return res.serverError('error');
  }
};
