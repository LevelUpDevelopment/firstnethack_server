module.exports = {

  isArray: (a) => {
      return (!!a) && (a.constructor === Array);
  },

  isObject: (a) => {
    return (!!a) && (a.constructor === Object);
  },

  escapeRegExpSpecialCharacters: (text) => {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
  },

  buildBaseTuple: (headItem, restItem) => {
    return [headItem, restItem];
  },

  addToTuple: (headItem, restItem) => {
    return [headItem, ...restItem];
  },
  
  getRandomInt: (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },

  trim: (string) => {
    const rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    return string.replace(rtrim, '');
  },

  jsUcfirst: (string) => { 
    return string.charAt(0).toUpperCase() + string.slice(1);
  },

  escapeJson: (json, exclusions) => {
    var escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
    var meta = {    // table of character substitutions
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"' : '\\"',
                '\\': '\\\\'
              };

    escapable.lastIndex = 0;
    return escapable.test(json) ? json.replace(escapable, function (a) {
        if ((!exclusions) || (exclusions && !exclusions.includes(a))) {
          var c = meta[a];
        } else {
          var c = a;
        }
        return (typeof c === 'string') ? c
          : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
    }) : json;
  },

  queryMapper: (obj, column, strict = false) => {
    const result = obj[column.toLowerCase()];
    if(result) {
      const results = result.split('.');
      if(results.length === 1) return result;
      return `${results[0]}.[${results[1]}]`;
    } else if (!strict) {
      return `[${column}]`;
    } else {
      return null
    }
  }

}