const _ = require('lodash');
const Promise = require('bluebird');
const common = require('./utils/common.js');

module.exports = {

  hasRole(user, roles) {
    if (user && user.roles) {
      const userRoles = user.roles;
      if (userRoles) {
        let hasRole = false;
        let hasThisRole = false;
        for (let r = 0; r < roles.length; r++) {
          hasThisRole = _.find(userRoles, { name: roles[r] }) ? true : false;
          if (hasThisRole) {
            hasRole = true;
          }
        }
        return hasRole;
      }
    }
    return false;
  },

  hasPermission(user, permissions) {
    let hasPermission = false;
    if (typeof permissions === 'string') {
      permissions = [permissions];
    }
    return new Promise((resolve, reject) => {
      if (common.isArray(permissions) && permissions.length > 0 && user && common.isArray(user.roles) && user.roles.length > 0) {
        const userRolesIds = user.roles.map((role) => {
          return role.id;
        });
        return this.findRoles(userRolesIds).then((rolesWithPermissions) => {
          rolesWithPermissions = _.flatten(rolesWithPermissions);
          for (let p = 0; p < permissions.length; p++) {
            for (let rp = 0; rp < rolesWithPermissions.length; rp++) {
              if (_.find(rolesWithPermissions[rp].permissions, { name: permissions[p] })) {
                hasPermission = true;
              }
            }
          }
          if (hasPermission) {
            return resolve();
          } else {
            return reject();
          }
        }).catch((err) => {
          return reject(err);
        });
      }
    })
  },

  findRoles(roleIds) {
    return new Promise((resolve, reject) => {
      if (common.isArray(roleIds) && roleIds.length > 0) {
        return Promise.map(roleIds, (roleId) => {
          return new Promise((resolveMap, rejectMap) => {
            return Role.find({ where: { id: roleId }, include: [{ as: 'permissions', model: Permission }] }).then((rawRoles) => {
              const roles = rawRoles.toJSON();
              return resolveMap(roles);
            }).catch((err) => {
              return rejectMap(err);
            });
          });
        }, {
          concurrency: 10
        }).then((rolesPopulated) => {
          return resolve(rolesPopulated);
        }).catch((err) => {
          return reject(err);
        });
      }
      return resolve(false);
    });
  }

 };
