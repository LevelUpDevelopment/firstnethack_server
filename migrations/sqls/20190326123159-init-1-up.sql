/****** Object:  Table [dbo].[permission]    Script Date: 5/9/2019 5:22:57 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[permission](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](55) NULL,
	[description] [varchar](200) NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[permission_Audit]    Script Date: 5/9/2019 5:22:57 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[permission_Audit](
	[id] [bigint] NULL,
	[name] [varchar](55) NULL,
	[description] [varchar](200) NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
	[AuditDataState] [varchar](10) NULL,
	[AuditDMLAction] [varchar](10) NULL,
	[AuditUser] [sysname] NULL,
	[AuditDateTime] [datetime] NULL,
	[UpdateColumns] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[role]    Script Date: 5/9/2019 5:22:58 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[role](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](55) NULL,
	[description] [varchar](200) NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[role_Audit]    Script Date: 5/9/2019 5:22:58 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[role_Audit](
	[id] [bigint] NULL,
	[name] [varchar](55) NULL,
	[description] [varchar](200) NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
	[AuditDataState] [varchar](10) NULL,
	[AuditDMLAction] [varchar](10) NULL,
	[AuditUser] [sysname] NULL,
	[AuditDateTime] [datetime] NULL,
	[UpdateColumns] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[rolepermission]    Script Date: 5/9/2019 5:22:58 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[rolepermission](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[roleId] [bigint] NULL,
	[permissionId] [bigint] NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[rolepermission_Audit]    Script Date: 5/9/2019 5:22:58 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[rolepermission_Audit](
	[id] [bigint] NULL,
	[roleId] [bigint] NULL,
	[permissionId] [bigint] NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
	[AuditDataState] [varchar](10) NULL,
	[AuditDMLAction] [varchar](10) NULL,
	[AuditUser] [sysname] NULL,
	[AuditDateTime] [datetime] NULL,
	[UpdateColumns] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[token]    Script Date: 5/9/2019 5:22:59 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[token](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[user] [varchar](max) NULL,
	[token] [varchar](max) NULL,
	[successful] [bit] NULL,
	[revoke] [bit] NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[user]    Script Date: 5/9/2019 5:22:59 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[user](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[email] [varchar](250) NULL,
	[password] [varchar](max) NULL,
	[active] [bit] NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
	[isLoggedIn] [bit] NULL,
	[isLockedOut] [bit] NULL,
	[timeStampOfLastLockOut] [datetimeoffset](7) NULL,
	[numberOfFailedLoginAttempts] [int] NULL,
	[numberOfLoginAttemptsRemaining] [int] NULL,
	[waitTimeToLogIn] [int] NULL,
	[nextWaitTimeToLogIn] [int] NULL,
	[username] [varchar](max) NULL,
	[firstname] [varchar](55) NULL,
	[lastname] [varchar](55) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[user_Audit]    Script Date: 5/9/2019 5:22:59 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[user_Audit](
	[id] [bigint] NULL,
	[email] [varchar](250) NULL,
	[password] [varchar](max) NULL,
	[active] [bit] NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
	[isLoggedIn] [bit] NULL,
	[isLockedOut] [bit] NULL,
	[timeStampOfLastLockOut] [datetimeoffset](7) NULL,
	[numberOfFailedLoginAttempts] [int] NULL,
	[numberOfLoginAttemptsRemaining] [int] NULL,
	[waitTimeToLogIn] [int] NULL,
	[nextWaitTimeToLogIn] [int] NULL,
	[username] [varchar](max) NULL,
	[firstname] [varchar](55) NULL,
	[lastname] [varchar](55) NULL,
	[AuditDataState] [varchar](10) NULL,
	[AuditDMLAction] [varchar](10) NULL,
	[AuditUser] [sysname] NULL,
	[AuditDateTime] [datetime] NULL,
	[UpdateColumns] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[userrole]    Script Date: 5/9/2019 5:22:59 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[userrole](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[roleId] [bigint] NULL,
	[userId] [bigint] NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[userrole_Audit]    Script Date: 5/9/2019 5:22:59 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[userrole_Audit](
	[id] [bigint] NULL,
	[roleId] [bigint] NULL,
	[userId] [bigint] NULL,
	[updatedAt] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NULL,
	[AuditDataState] [varchar](10) NULL,
	[AuditDMLAction] [varchar](10) NULL,
	[AuditUser] [sysname] NULL,
	[AuditDateTime] [datetime] NULL,
	[UpdateColumns] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


