const queue = require('./processes/queue.js');
const deadLetterQueue = require('./processes/deadLetterQueue.js');
const job = require('./processes/job.js');
// import { SSL_OP_LEGACY_SERVER_CONNECT } from 'constants';

var Sails = require('sails').constructor;
var sailsApp = new Sails();
var zmq = require('zeromq');
var sock = zmq.socket('sub');

const processes = {
  queue,
  deadLetterQueue,
  job
}

const IsJsonString = (str) => {
  try {
      JSON.parse(str);
  } catch (e) {
      return false;
  }
  return true;
}

sailsApp.load({
  hooks: {
    blueprints: false,
    controllers: false,
    cors: false,
    csrf: false,
    grunt: false,
    http: false,
    i18n: false,
    //orm: leave default hook
    policies: false,
    pubsub: false,
    request: false,
    responses: false,
    //services: leave default hook,
    session: false,
    sockets: false,
    views: false,
    orm: false,
    pubsub: false
    //"custom-hooks-that-are-not-needed-in-the-job-runner": false
  },
  environment: process.env.NODE_ENV
}, function(err) {
    // TODO: use env var to run appropriate process code.
    const args = (IsJsonString(process.env.args)) ? JSON.parse(process.env.args) : process.env.args;
    processes[process.env.process].start(args);

});