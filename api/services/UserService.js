const passport = require('passport');
const moment = require('moment');
const Promise = require('bluebird');

// default in case cannot find default settings
const attemptsPerFail_default = 3; // these are default
const timeMultiplier_default = 5; // this will applied to the lock out wait time


module.exports = {

  async getUserWithPermissions(email) {
    try {
      const rawUser = await User.findOne({
        where: { email: email }, 
        attributes: [
          'firstname',
          'lastname',
          'email',
          'id',
          'password',
        ],
        include: [
          { 
            model: Role, 
            as: 'roles', 
            attributes: ['id', 'name'],
            include: [
              { 
                model: Module,
                attributes: ['id', 'name', 'shortName'],
              }, 
              {
                model: Permission, 
                as: 'permissions',
                attributes: ['id', 'name']
              }
            ] 
          }
        ]
      });
      return rawUser.toJSON();
    } catch (err) {
      throw new Error(err);
    }
  },

  onPassportAuth(req, res, existingUser, error, user, info) {
    if (error) res.serverError(error);
    if (!user) { this.onFailedLoginAttempt(existingUser, req, res); }
    else {
      this.onSuccessfulLoginAttempt(user, req, res);
    }
  },

  doesUserExist(userEmail) {
    return new Promise((resolve, reject) => {
      return User.findOne({ 
        where: {
          email: userEmail
        }
      }).then((user) => {
        if (user && user.email) {
          return resolve(user);
        }
        return resolve(false);
      }).catch((err) => {
        return reject(err);
      })
    });
  },

  fibonacci(num) {
    var a = 1, b = 0, temp;

    while (num >= 0){
      temp = a;
      a = a + b;
      b = temp;
      num--;
    }

    return b;
  },

  getDefaultSettings() {
    return new Promise((resolve, reject) => {
      let attemptsPerFail = 3;
      let timeMultiplier = 2;

      if(sails.config.jwtSettings){
        attemptsPerFail = sails.config.jwtSettings.attemptsPerFail ? sails.config.jwtSettings.attemptsPerFail : attemptsPerFail_default;
        timeMultiplier = sails.config.jwtSettings.timeMultiplier ? sails.config.jwtSettings.timeMultiplier : timeMultiplier_default;
      }
      resolve({ attemptsPerFail: attemptsPerFail, timeMultiplier: timeMultiplier });
    });
  },

  getWaitTimeLeft(user) {
    return new Promise((resolve, reject) => {
      this.getDefaultSettings().then((defaultSettings) => {
        let numAttempts = user.numberOfFailedLoginAttempts;
        let waitTimeToLogIn = this.fibonacci(numAttempts / defaultSettings.attemptsPerFail) * defaultSettings.timeMultiplier;
        let timeStampOfLastLockOut = moment(user.timeStampOfLastLockOut);
        let secondsSinceLastLockOut = moment(parseInt(timeStampOfLastLockOut._i)).diff(moment.now(), 'seconds');
        resolve(waitTimeToLogIn - Math.abs(secondsSinceLastLockOut));
      });
    });
  },

  onFailedLoginAttempt(user, req, res) {
    this.getDefaultSettings().then((defaultSettings) => {
      user.numberOfFailedLoginAttempts = user.numberOfFailedLoginAttempts + 1;
      user.numberOfLoginAttemptsRemaining = user.numberOfFailedLoginAttempts % defaultSettings.attemptsPerFail;
      user.numberOfLoginAttemptsRemaining = user.numberOfLoginAttemptsRemaining != 0 ? Math.abs(defaultSettings.attemptsPerFail - user.numberOfLoginAttemptsRemaining) : 0;
      user.isLockedOut = user.isLockedOut;
      user.waitTimeToLogIn = this.fibonacci(user.numberOfFailedLoginAttempts / defaultSettings.attemptsPerFail) * defaultSettings.timeMultiplier;
      user.nextWaitTimeToLogIn = this.fibonacci((user.numberOfFailedLoginAttempts / defaultSettings.attemptsPerFail) + 1) * defaultSettings.timeMultiplier;
      user.timeStampOfLastLockOut = user.timeStampOfLastLockOut;

      if(user.numberOfLoginAttemptsRemaining === 0) {
        user.timeStampOfLastLockOut = moment();
        user.isLockedOut = true;
        user.cbLiteDbKey = null;
      } else {
        user.isLockedOut = false;
      }

      return User.update({
        numberOfFailedLoginAttempts: user.numberOfFailedLoginAttempts,
        numberOfLoginAttemptsRemaining: user.numberOfLoginAttemptsRemaining,
        isLockedOut: user.isLockedOut,
        timeStampOfLastLockOut: user.timeStampOfLastLockOut,
        waitTimeToLogIn: user.nextWaitTimeToLogIn,
        nextWaitTimeToLogIn: user.nextWaitTimeToLogIn,
        cbLiteDbKey: user.cbLiteDbKey
      },
      {
        where: {
          email: user.email
        }
      }).then(() => {
        return Token.create({
          user: user.id,
          token: null,
          successful: false,
          revoke: true
        }).then(() => {
          return  res.send('401', {
            failedLoginAttempt: {
              numberOfLoginAttemptsRemaining: user.numberOfLoginAttemptsRemaining,
              isLockedOut: user.isLockedOut,
              waitTimeToLogIn: user.waitTimeToLogIn,
              nextWaitTimeToLogIn: user.nextWaitTimeToLogIn
            }
          });
        });
      });
    }).catch((err) => {
      return res.serverError(err);
    });
  },

  onSuccessfulLoginAttempt(user, req, res) {
    this.getDefaultSettings().then((defaultSettings) => {
      const newToken = CipherService.createToken(user);
      user.token = newToken;
      return User.update({
        isLoggedIn: true,
        numberOfFailedLoginAttempts: 0,
        numberOfLoginAttemptsRemaining: defaultSettings.attemptsPerFail,
        isLockedOut: false,
        waitTimeToLogIn: 0,
        nextWaitTimeToLogIn: 0
      }, {
        where: { email: user.email }
      }).then(() => {
        const tokenTimeout = moment().add(sails.config.token.timeOutQuantity, sails.config.token.timeOutUnits).format('x');
        console.log(tokenTimeout);
        return Token.create({
          user: user.id,
          token: newToken,
          successful: true,
          revoke: false
        }).then(() => {
          return res.ok({
            token: newToken,
            user: user,
            expires: tokenTimeout,
            userAlreadyLoggedIn: user.isLoggedIn
          });
        });
      });
    });
  },

  attemptSignIn(req, res) {
    let lowercaseEmail = req.body.email.trim().toLowerCase();
    return this.doesUserExist(lowercaseEmail).then((existingUser) => {
      if (existingUser) {
        return Token.update({ revoke: true }, { where: { user: existingUser.id, revoke: false }}).then(() => {
          return this.getWaitTimeLeft(existingUser).then((waitTime) => {
            if (waitTime > 0) {
              return res.send('401', {
                failedLoginAttempt: {
                  numberOfLoginAttemptsRemaining: existingUser.numberOfLoginAttemptsRemaining,
                  waitTimeToLogIn: waitTime,
                  isLockedOut: existingUser.isLockedOut,
                  nextWaitTimeToLogIn: existingUser.nextWaitTimeToLogIn
                }
              });
            } else {
              passport.authenticate('user-local', this.onPassportAuth.bind(this, req, res, existingUser))(req, res);
            }
          }).catch((err) => {
            return res.serverError(err);
          });
        });
      } else {
        return res.unauthorized("Invalid User Name");
      }
    }).catch((err) => {
      return res.serverError(err);
    });
  }
};
