FROM node:8.11.3

# Configure environment
ENV APPDIR /data/app
EXPOSE 1338

# install pdfunite
RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y poppler-utils

# Update npm
RUN npm install -g npm@6.1.0 && npm cache clean -f

# Install grunt-cli globally for build
RUN npm install -g grunt-cli

# Install forever globally for process management
RUN npm install -g forever

# Copy application code
ADD . $APPDIR/
WORKDIR $APPDIR

# Install dependencies and build
RUN npm install

CMD ["forever", "app.js"]