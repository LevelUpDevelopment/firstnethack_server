'use strict';

/**
 * Passport configuration file where you should configure strategies
 */
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const CustomStrategy = require('passport-custom').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const EXPIRES_IN = 60 * 60 * 12;
const SECRET = process.env.tokenSecret || 'AC84udj5bl4k0In3Iyj4fXEZkdZdfNg7jO41EAtl205F7TO5fDdnO';
const ALGORITHM = 'HS256';
const ISSUER = 'localhost';
const AUDIENCE = 'localhost';

passport.use('user-local', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: false
}, async (email, password, next) => {
  try {
    const user = await AuthService.getUserWithPermissions(email);
    
    if (!user) {
      return next(null, false, {
        code: 'E_USER_NOT_FOUND',
        message: email + ' is not found'
      });
    }

    // TODO: replace with new cipher service type
    if (!CipherService.comparePassword(password, user.password)) {
      return next(null, false, {
        code: 'E_WRONG_PASSWORD',
        message: 'Password is wrong'
      });
    }

    delete user.password;
    return next(null, user, {});
  } catch (err) {
    return next(err, false, {});
  }
}));

passport.use('user-jwt', new JwtStrategy({
  secretOrKey: SECRET,
  issuer: ISSUER,
  audience: AUDIENCE,
  passReqToCallback: true,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}, async (req, payload, next) => {
  try {
    return next(null, payload.user, {});
  } catch (err) {
    return next(err, false, {});
  }
}));

module.exports.jwtSettings = {
  attemptsPerFail: 3,
  timeMultiplier: 2,
  maxOrgFailureAttempts: 10,
  superAdminRole: 'superadmin',
  expiresIn: EXPIRES_IN,
  secret: SECRET,
  algorithm: ALGORITHM,
  issuer: ISSUER,
  audience: AUDIENCE
};
