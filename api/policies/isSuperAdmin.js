/**
 * isSuperAdmin
 * @description :: Policy to determine if user is a super admin
 */

const passport = require('passport');

module.exports = function (req, res, next) {
  const user = req.user;
  if (user && RoleService.hasRole(user, ['superadmin'])) {
    return next();
  }
  return res.unauthorized(null, 403, 'User not defined.');
};