module.exports = {
  attributes: {
    name: {
      type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    },
    moduleId: {
      type: Sequelize.BIGINT
    }
  },
  associations: function () {
    Role.belongsToMany(Permission, { as: 'permissions', through: RolePermission, foreignKey: 'roleId', otherKey: 'permissionId' });
    Role.belongsTo(Module);
  },
  options: {
    freezeTableName: false,
    tableName: 'role',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
    hasTrigger: true
  }
};