#!/bin/bash

NETWORK_NAME="inrdocker"
SERVER_IP="172.19.0.4"
NETWORK_SUBNET="172.19.0.0/16"
IMAGE_NAME="mongo"
IMAGE_TAG="latest"
CONTAINER_NAME='AlphaConcepts-mac-mongo'
PORT="27017"
DOCKER_DATA_VOLUME="AlphaConcepts-mongo-data"
CONTAINER_DATA_DIRECTORY="/data/db"

docker network ls | grep -v Exit | grep -q $NETWORK_NAME
if [ $? -ne 0 ]; then
    $(docker network create --subnet=$NETWORK_SUBNET $NETWORK_NAME)
fi

docker pull $IMAGE_NAME:$IMAGE_TAG
docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME
docker rmi $IMAGE_NAME:current
docker tag $IMAGE_NAME:$IMAGE_TAG $IMAGE_NAME:current
docker volume rm $DOCKER_DATA_VOLUME
docker volume create $DOCKER_DATA_VOLUME
docker run \
--name $CONTAINER_NAME \
--net $NETWORK_NAME \
--ip $SERVER_IP \
-v $DOCKER_DATA_VOLUME:$CONTAINER_DATA_DIRECTORY \
--publish $PORT:27017 \
--detach $IMAGE_NAME:current