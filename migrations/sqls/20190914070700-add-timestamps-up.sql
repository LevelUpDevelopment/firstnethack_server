/* Replace with your SQL commands */
ALTER TABLE incident
ADD updatedAt datetimeoffset;

ALTER TABLE incident
ADD createdAt datetimeoffset;

ALTER TABLE incidentComment
ADD createdAt datetimeoffset;

ALTER TABLE incidentComment
ADD updatedAt datetimeoffset;