read -p "Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	docker kill $(docker ps -q)
	docker rm $(docker ps -a -q)
fi
