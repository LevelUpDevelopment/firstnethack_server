'use strict';

const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

module.exports = {
  secret: sails.config.jwtSettings.secret,
  issuer: sails.config.jwtSettings.issuer,
  audience: sails.config.jwtSettings.audience,

  /**
   * Hash the password field of the passed user.
   */
  hashPassword: function (password) {
    if (password) {
      const hashedPassword = bcrypt.hashSync(password);
      return hashedPassword;
    }
    return null;
  },
 
  /**
   * Compare user password hash with unhashed password
   * @returns boolean indicating a match
   */
  comparePassword: function(passwordFromReq, passwordFromDb) {
    return bcrypt.compareSync(passwordFromReq, passwordFromDb);
  },
 
  /**
   * Create a token based on the passed user
   * @param user
   */
  createToken: function(user) {
    return jwt.sign({
        user: user
      },
      sails.config.jwtSettings.secret,
      {
        algorithm: sails.config.jwtSettings.algorithm,
        expiresIn: sails.config.jwtSettings.expiresIn,
        issuer: sails.config.jwtSettings.issuer,
        audience: sails.config.jwtSettings.audience
      }
    );
  },

  hashString(hash) {
    return new Promise((resolve, reject) => {
      return bcrypt.genSalt(10, function(saltError, salt) {
        if (saltError) {
          return reject(saltError);
        }
        return bcrypt.hash(hash, salt, null, (hashError, hash) => {
          if (hashError) {
            return reject(hashError);
          }
          return resolve(hash);
        });
      });
    });
  }

};
