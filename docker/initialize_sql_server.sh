#!/bin/bash

NETWORK_NAME="AlphaConceptsdocker"
SERVER_IP="172.18.0.4"
IMAGE_USER="microsoft"
IMAGE_NAME="mssql-server-linux"
IMAGE_TAG="2017-latest"
CONTAINER_NAME='AlphaConcepts-mac-sql'
PORT="1433"
DATABASE_NAME="AlphaConcepts"
SA_PASSWORD="cz7E2J7@8UW7"
DOCKER_DATA_VOLUME="AlphaConcepts-sql-data"
CONTAINER_DATA_DIRECTORY="/var/opt/mssql/data/"
CONTAINER_BACKUP_DIRECTORY="/var/opt/mssql/"

docker pull $IMAGE_USER/$IMAGE_NAME:$IMAGE_TAG
docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME
docker rmi $IMAGE_USER/$IMAGE_NAME:current
docker tag $IMAGE_USER/$IMAGE_NAME:$IMAGE_TAG $IMAGE_USER/$IMAGE_NAME:current
docker volume rm $DOCKER_DATA_VOLUME
docker volume creae $DOCKER_DATA_VOLUME
docker run \
--name $CONTAINER_NAME \
-v $DOCKER_DATA_VOLUME:$CONTAINER_DATA_DIRECTORY \
-e "ACCEPT_EULA=Y" \
-e "SA_PASSWORD="$SA_PASSWORD \
--publish $PORT:1433 \
--detach $IMAGE_USER/$IMAGE_NAME:current
docker exec $CONTAINER_NAME /opt/mssql-tools/bin/sqlcmd \
-S localhost -U SA -P $SA_PASSWORD \
-Q 'USE master; CREATE DATABASE AlphaConcepts; CREATE DATABASE AlphaConcepts_unitTest;'
