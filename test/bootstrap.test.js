/* eslint prefer-arrow-callback: "off" */
/* eslint func-style: "off" */
/* eslint no-use-before-define: "off" */
/* eslint no-underscore-dangle: "off" */
// Passing arrow functions (“lambdas”) to Mocha is discouraged. Due to the lexical binding of this,
// such functions are unable to access the Mocha context.
'use strict';

const Sails = require('sails');
const sequelize_fixtures = require('sequelize-fixtures');
const TestHelper = require('./TestHelper.test.js');


/**
 * Runs before/after the entire suite of unit tests.
 */
before((done) => {
  console.log(`Lifting sails to run unit tests with environment development...`);

  return Sails.lift({
    log: {
      level: 'debug'
    },
    models: {
      connection: 'unitTest'
    }
  }, (err, server) => {
    if (err) {
      console.log('There was an error lifting sails.')
      return done(err);
    }
    console.log('Sails successfully lifted.');
    sails = server;
    // Helper functions for use in all tests
    sails.TestHelper = TestHelper;
    // Fixtures loaded in case needed.
    console.log('Loading fixtures...');
    return sails.TestHelper.getFixtures().then((fixtures) => {
      sails.fixtures = fixtures;

      return sails.TestHelper.truncateModels(sails.models).then(() => {
        // Load fixtures


        return sequelize_fixtures.loadFile('test/fixtures/*.json', sails.models, {
          modifyFixtureDataFn: (data) => {
            if(!data.createdAt) {
              data.createdAt = new Date();
            }
            if(!data.updatedAt) {
              data.updatedAt = new Date();
            }
            return data;
          }
        }).then((response) => {
          console.log('Fixtures Loaded.');
          return done(err, sails);
        });
      });
    });
  });
});

after((done) => {
  sails.lower(done);
});
