/* Replace with your SQL commands */
insert into role
(name, description, updatedAt, createdAt, moduleId)
VALUES
('superadmin', 'Super Admin with all modules', GETDATE(), GETDATE(), null);

insert into [permission]
(name, description, updatedAt, createdAt)
VALUES
('create_user', 'Can create users', GETDATE(), GETDATE());

insert into [rolepermission]
(roleId, permissionId, updatedAt, createdAt)
VALUES (
  (select id from role where name = 'superadmin'),
  (select id from permission where name = 'create_user'),
  GETDATE(),
  GETDATE()
);
