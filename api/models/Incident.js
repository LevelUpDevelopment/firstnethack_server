module.exports = {
  attributes: {
    caseNumber: {
      type: Sequelize.STRING,
    },
    coords: {
      type: Sequelize.STRING,
    },
    type: {
      type: Sequelize.STRING,
    },
    dts: {
      type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    },
    userId: {
      type: Sequelize.STRING
    },
  },
  associations: function () {
    Incident.hasMany(IncidentComment, { as: 'incidentComments', foreignKey: 'incidentId' });
  },
  options: {
    freezeTableName: false,
    tableName: 'incident',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
    hasTrigger: true
  }
};