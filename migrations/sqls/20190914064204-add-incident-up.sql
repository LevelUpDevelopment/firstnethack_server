/* Replace with your SQL commands */

CREATE TABLE incident (
    type nvarchar(200),
    dts datetime,
    description nvarchar(500),
);

CREATE TABLE incidentComment (
    incidentId nvarchar(200),
    comment nvarchar(500),
);