module.exports = {
  attributes: {
    user: {
      type: Sequelize.STRING,
      allowNull: false
    },
    token: {
      type: Sequelize.STRING
    },
    successful: {
      type: Sequelize.BOOLEAN
    },
    revoke: {
      type: Sequelize.BOOLEAN
    }
  },
  associations: function() {
  },
  options: {
    tableName: 'token',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
    scopes: {}
  }
};
