module.exports = {
  attributes: {
    incidentId: {
      type: Sequelize.STRING,
    },
    comment: {
      type: Sequelize.STRING
    },
  },
  associations: function () {
    IncidentComment.belongsTo(Incident, { as: 'incidentComments', foreignKey: 'incidentId' });
  },
  options: {
    freezeTableName: false,
    tableName: 'incidentComment',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
    hasTrigger: true
  }
};