/**
 * IncidentController
 * @description :: Server-side logic for managing user's authorization
 */
const moment = require("moment");
const Promise = require('bluebird');
/**
 * Triggers when user authenticates via passport
 * @param {Object} req Request object
 * @param {Object} res Response object
 * @param {Object} error Error object
 * @param {Object} user User profile
 * @param {Object} info Info if some error occurs
 * @private
 */

module.exports = {
  /**
   * creates an incident
   * @param {Object} req Request object
   * @param {Object} res Response object
   */

  createIncident: (req, res) => {
    const caseNumber = req.body.caseNumber;
    const coords = req.body.coords;
    const userId = req.body.userId;
    const type = req.body.type;

    if (!caseNumber || !coords || !userId || !type) {
      return res.badRequest();
    }

    return Incident.create({ caseNumber, coords, userId, type }).then((createdIncident) => {
      return Incident.find({
        where: {
          id: createdIncident.id
        },
        include: [
          {
            model: IncidentComment,
            as: 'incidentComments',
          },
        ]
      }).then((incidentRaw) => {
        const incident = incidentRaw.toJSON();
        return res.ok(incident);
      });
    }).catch((err) => res.serverError(err));
  },

  updateIncident: (req, res) => {
    console.log('oh hi');
    const id = req.body.id;
    const description = req.body.description;

    if (!description || !id) {
      return res.badRequest();
    }

    return Incident.update({ description }, { where: { id }}).then(() => {
      return Incident.find({
        where: {
          id
        },
        include: [
          {
            model: IncidentComment,
            as: 'incidentComments',
          },
        ]
      }).then((incidentRaw) => {
        const incident = incidentRaw.toJSON();
        return res.ok(incident);
      });
    }).catch((err) => res.serverError(err));

  },

  createIncidentComment: (req, res) => {
  	const incidentId = req.body.incidentId;
  	const comment = req.body.comment;

    if (!comment || !incidentId) {
      return res.badRequest();
    }
    
  	return IncidentComment.create({ incidentId, comment }).then(() => {
      return Incident.find({
        where: {
          id: incidentId
        },
        include: [
          {
            model: IncidentComment,
            as: 'incidentComments',
          }
        ]
      }).then((incidentRaw) => {
        const incident = incidentRaw.toJSON();
        return res.ok(incident);
      });
    }).catch((err) => res.serverError(err));
  },

  getIncident: (req, res) => {
    const incidentId = req.body.incidentId;

    return Incident.find({
      where: { 
        id: incidentId
      },
      include: [
        {
          model: IncidentComment,
          as: 'incidentComments',
        },
      ]
    }).then((incidentRaw) => {
      const incident = incidentRaw.toJSON();
      return res.ok(incident);
    }).catch((err) => {
      return res.serverError(err);
    })
  },

  getIncidents: (req, res) => {

    return Incident.findAll({
      include: [
        {
          model: IncidentComment,
          as: 'incidentComments',
        },
      ]
    }).then((incidents) => {
      incidents = incidents.map((i) => {
        return i.toJSON();
      });
      return res.ok(incidents);
    }).catch((err) => {
      return res.serverError(err);
    })
  },

  loadMockIncidents: (req, res) => {
    const amount = req.body.amount || 200;

    const typesPool = [
      {
        name: 'Opioids and other drug related',
        key: 'opioids',
        icon: 'blue',
      }, {
        name: 'Car Theft',
        key: 'cartheft',
        icon: 'brown',
      }, {
        name: 'Breaking and Entering',
        key: 'breakingandentering',
        icon: 'darkGreen',
      }, {
        name: 'Petty Theft',
        key: 'pettytheft',
        icon: 'greenYellow',
      }, {
        name: 'Robbery',
        key: 'robbery',
        icon: 'lightGreen',
      }, {
        name: 'Armed Robery',
        key: 'armedrobery',
        icon: 'lightOrange',
      }, {
        name: 'Armed Assault',
        key: 'armedassault',
        icon: 'lightYellow',
      }, {
        name: 'Missing Person',
        key: 'missingperson',
        icon: 'purple',
      }, {
        name: 'Amber Alert',
        key: 'amberalert',
        icon: 'ping',
      }, {
        name: 'Silver Alert',
        key: 'silveralert',
        icon: 'red',
      }, {
        name: 'Blue Alert',
        key: 'bluealert',
        icon: 'teal',
      }, {
        name: 'Domestic Violence',
        key: 'domesticviolence',
        icon: 'yellow',
      },
    ];

    let incidents = [];
    for (let a = 0; a < amount; a++) {
      const caseNumber = `s${Math.floor(Math.random() * 999999999).toString()}`;
      const coords = `[${CoordsService.createCoords().toString()}]`;
      const userId = 9999999999;
      const type = typesPool[Math.floor(Math.random() * 12)].key;
      const description = 'System Generated - for display purposes only.';
      incidents.push({
        caseNumber,
        coords,
        userId,
        type,
        description,
      });
    }

    return Promise.map(incidents, (incident) => {
      return new Promise((resolveMap, rejectMap) => {
        return Incident.create(incident).then(() => {
          return resolveMap();
        }).catch((err) => {
          return rejectMap(err);
        });
      });
    }, {
      concurrency:  3
    }).then(() => {
      return res.ok();
    }).catch((err) => {
      return res.serverError(err);
    })
  }

}