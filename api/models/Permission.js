module.exports = {
  attributes: {
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    description: {
      type: Sequelize.STRING
    }
  },
  associations: function () {
    Permission.belongsToMany(Role, { as: 'roles', through: RolePermission, foreignKey: 'permissionId', otherKey: 'roleId' });
  },
  options: {
    freezeTableName: false,
    tableName: 'permission',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
    hasTrigger: true
  }
};