/* Replace with your SQL commands */
CREATE TABLE [dbo].[module](
    [id] [bigint] IDENTITY(1,1) PRIMARY KEY NOT NULL,
    [name] varchar(255),
    [shortName] varchar(100),
    createdAt datetimeoffset,
    updatedAt datetimeoffset,
)
