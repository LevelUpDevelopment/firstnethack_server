module.exports = {
  apps: [
    {
      name: "AlphaConcepts-Server",
      script: "app.js",

      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
      node_args: ["--inspect"],
      instances: 1,
      autorestart: true,
      watch: true
    }
  ]
};
