module.exports = {
  attributes: {
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    }
  },
  associations: function () {
  },
  options: {
    freezeTableName: false,
    tableName: 'rolepermission',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
    hasTrigger: true
  }
};