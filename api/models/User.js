module.exports = {
  attributes: {
    email: {
      type: Sequelize.STRING,
      allowNull: false
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    firstname: {
      type: Sequelize.STRING
    },
    lastname: {
      type: Sequelize.STRING
    },
    address: {
      type: Sequelize.STRING
    },
    addressLine2: {
      type: Sequelize.STRING
    },
    city: {
      type: Sequelize.STRING
    },
    state: {
      type: Sequelize.STRING
    },
    zip: {
      type: Sequelize.STRING
    }
  },
  associations:  () => {
    User.belongsToMany(Role, { as: 'roles', through: UserRole, foreignKey: 'userId', otherKey: 'roleId' });
    User.beforeCreate((user, options) => {
      let password = '!+vnEPjWa8V$n@Ya';
      if (user.password) {
        password = user.password;
      }
      const hashedPassword = CipherService.hashPassword(password);
      user.password = hashedPassword;
    });
  },
  options: {
    freezeTableName: false,
    tableName: 'user',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
    hasTrigger: true
  }
};