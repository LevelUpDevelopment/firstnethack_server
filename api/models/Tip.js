module.exports = {
  attributes: {
    description: {
      type: Sequelize.STRING,
    },
    coords: {
      type: Sequelize.STRING,
    },
  },
  associations: function () {
  },
  options: {
    freezeTableName: false,
    tableName: 'tip',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
    hasTrigger: true
  }
};