module.exports.sentry = {
  active: process.env.SENTRY_DSN ? true : false,
  dsn: process.env.SENTRY_DSN || null,
  options: {
    debug: false,
    environment: process.env.SENTRY_ENVIRONMENT || 'development'
  }
};