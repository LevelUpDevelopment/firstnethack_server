'use strict';

const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

module.exports = {
  createCoords: () => {
    const coordStarters = [
      {lat: -86.2, lon: 39.8 },
      {lat: -86.1, lon: 39.8 },
      {lat: -86.1, lon: 39.7 },
      {lat: -86.2, lon: 39.7 },
    ];
    const rando = Math.floor(Math.random() * 10);
    let lat = '';
    let lon = '';
    if (rando < 2) {
      lat = coordStarters[0].lat;
      lon = coordStarters[0].lon;
    } else if (rando < 5) {
      lat = coordStarters[1].lat;
      lon = coordStarters[1].lon;
    } else if (rando < 8) {
      lat = coordStarters[2].lat;
      lon = coordStarters[2].lon;
    } else {
      lat = coordStarters[3].lat;
      lon = coordStarters[3].lon;
    }
    const random1 = Math.floor(Math.random() * 10);
    const random2 = Math.floor(Math.random() * 10);
    const random3 = Math.floor(Math.random() * 10);
    const random4 = Math.floor(Math.random() * 10);
    const random5 = Math.floor(Math.random() * 10);
    const first = parseFloat(`${lat}${random1}${random2}${random3}${random4}${random5}`, 10);
    const second = parseFloat(`${lon}${random4}${random5}${random2}${random1}${random3}`, 10);
    return [first, second];
  }

};
