set -a
. ./.env
set +a

echo export DISABLE_SQL_LOG=true
echo export UNIT_TEST=true

[[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh

[[ -f ".nvmrc" ]] && nvm use

./node_modules/db-migrate/bin/db-migrate up -e unitTest --force-exit

DISABLE_SQL_LOG=true UNIT_TEST=true ./node_modules/mocha/bin/mocha --inspect ./test/bootstrap.test.js ./test/unit/**/*.test.js
