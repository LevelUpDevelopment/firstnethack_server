/* Replace with your SQL commands */

ALTER TABLE incident
ADD id bigint IDENTITY
CONSTRAINT PK_incident PRIMARY KEY CLUSTERED;

ALTER TABLE incidentComment
ADD id bigint IDENTITY
CONSTRAINT PK_incidentComment PRIMARY KEY CLUSTERED;