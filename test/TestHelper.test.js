'use strict';

const sails = require('sails');
const _ = require('lodash');
const Promise = require('bluebird');
const request = require('supertest-as-promised');
const fs = require('fs');
const path = require('path');
const utils = require('../api/services/utils/common.js');

const modelDeletionExclusion = ['group', 'joberror', 'reporttype', 'reporttemplate', 'status', 'statuscondition', 'statusgroup', 'statustree', 'role', 'permission', 'rolepermission'];

/**
 * Adds access token to a URI
 * @param  {String} uri         The URI to be accessed
 * @param  {String} accessToken The access token for authentication
 * @return {String}             Returns the URI with access token appended appropriately for authorized API call
 */
const tokenizeApiEndpoint = (uri, accessToken) => {
  sails.log('tokenizeApiEndpoint');
  return `${uri}?access_token=${accessToken}`;
};

/**
 * Returns an object with all fixtures
 * @return {Object}          Returns a JSON object for all fixtures
 */
const getFixtures = () => {
  return new Promise((resolve, reject) => {
    const fixtures = {};
    return fs.readdir(path.resolve(__dirname, 'fixtures'), (err, files) => {
      if(err) {
        console.log("Could not list the fixtures directory. No fixtures found.", err);
        return reject(err);
      }
      files.forEach((file, index) => {
        const model = file.replace('.json', '');
        fixtures[model] = require(path.resolve(__dirname, 'fixtures', `${model}.json`));
      });
      return resolve(fixtures);
    });
  });
};

/**
 * Returns an object with the fixturedata for a model
 * @param  {String} model         The model fixture to be accessed
 * @param  {String} criteria      A JSON object used for lodash filtering
 * @return {Object}          Returns a JSON object for a fixture
 */
const getFixture = (model, criteria) => {
  const fixture = require(`./fixtures/${model}.json`);
  return _.findOne(fixture, criteria);
};

/**
 * truncates/delets all data in passed models.
 * @param  {Array} models          Array containing models to truncate/delete
 * @return {Object}               Returns a promise
 */
const truncateModels = (models) => {
  const modelsToTruncate = [];
  for (let modelKey in models) {
    if (_.indexOf(modelDeletionExclusion, modelKey.toLowerCase()) === -1) {
      modelsToTruncate.push(modelKey);
    }
  }
  return new Promise((resolve, reject) => {
    return SequelizeConnections.default.query(`use AlphaConcepts_unitTest; EXEC sp_MSForEachTable "ALTER TABLE ? NOCHECK CONSTRAINT all";`).then(() => {
      return Promise.map(modelsToTruncate, (modelToTruncate) => {
        return new Promise((resolveMap, rejectMap) => {
          return SequelizeConnections.default.query(`use AlphaConcepts_unitTest; DELETE FROM [${modelToTruncate}]
           DBCC CHECKIDENT ([${modelToTruncate}], RESEED, 0)
          `).then(() => {
            return resolveMap();
          }).catch((err) => {
            return rejectMap(err);
          });
        });
      }, {
        concurrency: 3
      }).then(() => {
        return SequelizeConnections.default.query(`use AlphaConcepts_unitTest; exec sp_MSForEachTable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all";`).then(() => {
          return resolve();
        }).catch((err) => {
          console.log(err);
          return reject(err);
        });
      }).catch((err) => {
        console.log(err);
        return reject(err);
      })
    }).catch((err) => {
      console.log(err);
      return reject(err);
    });
  });
};

/**
 * Calls the authentication API that returns an access token for given user credentials.
 * @param  {Object} credentials { email: 'address', password: 'plainTextPassword' }
 * @return {Promise}             Returns a promise that will eventually return the received token
 */
const getUserToken = credentials => {
  sails.log('getUserToken request begin');
  return request(sails.hooks.http.app).post('/api/auth/login')
  .send(credentials)
  .then(response => {
    sails.log('getUserToken request response');
    return response.body.token;
  });
};

const getActiveAdminToken = () => {
  return getUserToken(activeAdminCredentials);
};

const getActiveSuperAdminToken = () => {
  return getUserToken(activeSuperAdminCredentials);
};

module.exports = {
  getActiveAdminToken,
  getActiveSuperAdminToken,
  getFixtures,
  getFixture,
  tokenizeApiEndpoint,
  truncateModels,
  getUserToken
};
