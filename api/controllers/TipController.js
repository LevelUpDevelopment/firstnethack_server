/**
 * TipController
 * @description :: Server-side logic for managing user's authorization
 */
const moment = require("moment");
/**
 * Triggers when user authenticates via passport
 * @param {Object} req Request object
 * @param {Object} res Response object
 * @param {Object} error Error object
 * @param {Object} user User profile
 * @param {Object} info Info if some error occurs
 * @private
 */

module.exports = {
  /**
   * creates a tip
   * @param {Object} req Request object
   * @param {Object} res Response object
   */

  createTip: (req, res) => {
    const description = req.body.description;
    const coords = req.body.coords;

    if (!caseNumber || !coords || !userId || !type) {
      return res.badRequest();
    }

    return Tip.create({ caseNumber, coords, userId, type }).then((createdTip) => {
      return Tip.find({
        where: {
          id: createdTip.id
        },
      }).then((tipRaw) => {
        const tip = tipRaw.toJSON();
        return res.ok(tip);
      });
    }).catch((err) => res.serverError(err));
  },

}