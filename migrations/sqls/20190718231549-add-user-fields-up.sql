alter table [user] drop column [active];
alter table [user] drop column [isLoggedIn];
alter table [user] drop column [isLockedOut];
alter table [user] drop column [timeStampOfLastLockOut];
alter table [user] drop column [numberOfFailedLoginAttempts];
alter table [user] drop column [numberOfLoginAttemptsRemaining];
alter table [user] drop column [waitTimeToLogin];
alter table [user] drop column [nextWaitTimeToLogin];
alter table [user] drop column [username];

alter table [user] add [Address] varchar(255);
alter table [user] add [AddressLine2] varchar(100);
alter table [user] add [City] varchar(255);
alter table [user] add [State] varchar(2);
alter table [user] add [Zip] varchar(5);