'use strict';

const should = require('should'); // eslint-disable-line no-unused-vars
const request = require('supertest-as-promised');
const HttpStatus = require('http-status-codes');

let standardRequest = {};

describe('AUTHCONTROLLER', () => {
  before(done => {
    standardRequest = request.agent(sails.hooks.http.app);
    done();
  });
  it('CREATE ADMIN', () => {
    return standardRequest
    .post('/api/v1/auth/createSuperAdmin')
    .set( { 'Authorization': process.env.CREATE_ADMIN_PASSWORD, 'Content-Type': 'application/json'} )
    .send({
      "password": "Test!Unit@2019",
      "email": "unitTester2@lud.com",
      "firstName": "our",
      "lastName": "hero",
      "roles": [
        "admin"
      ]
    })
    .expect(HttpStatus.OK)
  })

  it('SIGN IN should return an error saying either bad email or password', () => {
    return standardRequest
    .post('/api/v1/auth/login')
    .send({
      "email": 'notReal@email.com',
      "password": 'gdfgsdfg@gmail.com',
    })
    .expect(HttpStatus.UNAUTHORIZED)
    .expect(response => {
      response.should.have.property('error');
    });
  })

  it('SIGN IN should return an error saying either bad email or password', () => {
    return standardRequest
    .post('/api/v1/auth/login')
    .send({
      "email": 'gdfgsdfg@gmail.com',
      "password": 'badPasswordThatDOesNotExist!'
    })
    .expect(HttpStatus.UNAUTHORIZED)
    .expect(response => {
      response.should.have.property('error');
    });
  })


  it('SIGN IN should be ok and should return active user', () => {
    return standardRequest
    .post('/api/v1/auth/login')
    .send({
      "email": "unitTester2@lud.com",
      "password": "Test!Unit@2019"
    })
    .expect(HttpStatus.OK)
    .expect(response => {
      response.body.user.should.have.property('token');
      sails.token = response.body.token;
    });
  })
})