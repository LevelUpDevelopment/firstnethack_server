module.exports = {
    attributes: {
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
        },
        shortName: {
            type: Sequelize.STRING,
        },
        createdAt: {
            type: Sequelize.STRING,
        },
        updatedAt: {
            type: Sequelize.STRING,
        },

    },
    associations: function () {
        Module.beforeCreate((module, options) => {
            return {
                ...module,
                createdAt: new Date(),
                updatedAt: new Date()
            }
        });
        Module.beforeUpdate((module, options) => {
            return {
                ...module,
                updatedAt: new Date()
            }
        });
        Module.hasOne(Role)
    },
    options: {
        freezeTableName: false,
        tableName: 'module',
        classMethods: {},
        instanceMethods: {},
        hooks: {},
        hasTrigger: true
    }
};
