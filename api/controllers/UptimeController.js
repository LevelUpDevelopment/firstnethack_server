/**
 * UptimeController
 *
 * @description :: Server-side logic for determining if the app is working properly. Returns 200 for everything is OK, and 500 for Something Wrong.
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  checkDatabase: (req, res) => {
    const query = "select 'test' as test where 1 = 1";
    return SequelizeConnections.default.query(query, { raw: true }).then(() => {
      return res.ok();
    }).catch((err) => {
      return res.serverError(err);
    });
  }
}
