/**
 * isAuthenticated
 * @description :: Policy to inject user in req via JSON Web Token
 */
var passport = require('passport');

module.exports = (req, res, next) => {
  return passport.authenticate('user-jwt', (err, user, info) => {
    if (err) {
   	  return res.serverError(err);
    }
    if (!user) {
      return res.unauthorized(null, info && info.code, info && info.message);
    }
    req.user = user;

    return next();
  })(req, res);
};
