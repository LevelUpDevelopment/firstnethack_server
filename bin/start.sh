#!/bin/sh

# Some CONST setup related to project
MSSQL_DOCKER=AlphaConcepts-mac-sql
MSSQL_DOCKER_SH='./docker/initialize_sql_server.sh'
MONGO_DOCKER=AlphaConcepts-mac-mongo
MONGO_DOCKER_SH='./docker/initialize_mongo_server.sh'
CONTAINER_START_RETRY_ATTEMPTS=3
declare -a DOCKERS=("MSSQL" "MONGO")

MIN_NPM_VERSION=6.9.0

#TODO: Move commonly used functions to a support foler for re-use.
function does_docker_container_exist()
{
  retval=""
  if [ "$(docker ps -a | grep $1 | awk '{print $1}')" != "" ]; then
    retval="true"
  else
    retval="false"
  fi
  echo "$retval"
}

function is_docker_container_started()
{
  retval=""
  if [ "$(docker ps | grep $1 | awk '{print $1}')" != "" ]; then
    retval="true"
  else
    retval="false"
  fi
  echo "$retval"
}

function attempt_start_container()
{
  retval=""
  attempt=0
  if [ -z ${2+x} ]; then
    attempt=1
  else
    attempt=$2
  fi

  if (( attempt <= CONTAINER_START_RETRY_ATTEMPTS )); then
    container_id=$(docker ps -a | grep $1 | awk '{print $1}')
    docker start $container_id >/dev/null & wait
    if [ $? == 0 ]; then
      isStarted=$(is_docker_container_started $1)
      if [ "$isStarted" == "true" ]; then
        retval="true"
      else
        retval="false"
      fi
    else
      retval="false"
    fi
    if [ "$retval" == "false" ]; then
      new_attempt=$attempt+1
      attempt_start_container $1 $new_attempt
    fi
  fi

  echo "$retval"

}

function attempt_run_sh()
{
  echo "$1" >&2
  sh $1 & wait
}

function version_gte()
{
  retval=""
  major_req="$(cut -d'.' -f1 <<<"$1")"
  minor_req="$(cut -d'.' -f2 <<<"$1")"
  patch_req="$(cut -d'.' -f3 <<<"$1")"

  major_cur="$(cut -d'.' -f1 <<<"$2")"
  minor_cur="$(cut -d'.' -f2 <<<"$2")"
  patch_cur="$(cut -d'.' -f3 <<<"$2")"

  if [ "$3" == 'exact' ]; then
    if [ $((major_req)) -eq $((major_cur)) ] && [ $((minor_req)) -eq $((minor_cur)) ] && [ $((patch_req)) -eq $((patch_cur)) ]; then
      retval="true"
    else
      retval="false"
    fi
  fi

  if [ "$3" == 'major' ]; then
    if [ $((major_req-1)) -lt $((major_cur)) ]; then
      retval="true"
    else
      retval="false"
    fi
  fi

  if [ "$3" == 'minor' ]; then
    if [ $((major_req-1)) -lt $((major_cur)) ] && [ $((minor_req-1)) -lt $((minor_cur)) ]; then
      retval="true"
    else
      retval="false"
    fi
  fi

  if [ "$3" == 'patch' ]; then
    if [ $((major_req)) -eq $((major_cur)) ] && [ $((minor_req-1)) -lt $((minor_cur)) ] && [ $((patch_req-1)) -lt $((patch_cur)) ]; then
      retval="true"
    else
      retval="false"
    fi
  fi

  echo "$retval"
}

#SETUP UP ENVIRONMENT
npmVersionUptoDate=$(version_gte $MIN_NPM_VERSION $(npm -v) "minor")

if [ "$npmVersionUptoDate" == "false" ]; then
  echo "Initializing min version of npm: ${MIN_NPM_VERSION}..."
  version_string="npm@${MIN_NPM_VERSION}"
  npm install -g $version_string & wait
fi

set -a
. ./.env
set +a

[[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh

[[ -f ".nvmrc" ]] && nvm use

npm i

#START EACH AND EVERY DOCKER CONTAINER NEEDED IF NOT ALREADY STARTED
hadToInitializeDocker="false"

for docker in ${DOCKERS[@]}; do

  docker_substitution="${docker}_DOCKER"
  docker_sh_substitution="${docker}_DOCKER_SH"
  doesDockerExist=$(does_docker_container_exist ${!docker_substitution})

  if [ "$doesDockerExist" == "false" ]; then
    echo "Initializing ${docker}..."
    hadToInitializeDocker="true"
    attempt_run_sh ${!docker_sh_substitution}
  else
    echo "Found ${docker} container already existed..."
  fi

  isDockerStarted=$(is_docker_container_started ${!docker_substitution})

  if [ "$isDockerStarted" == "false" ]; then
    echo "Starting ${docker}..."
    didDockerStart=$(attempt_start_container ${!docker_substitution})
    if [ "$didDockerStart" == "true" ]; then
      echo "${docker} Started..."
    else
      echo "${docker} Failed to Start. This is a permanent error. Exiting." >&2
      exit 1
    fi
  else
    echo "Found ${docker} container was already started..."
  fi

done

if [ "$hadToInitializeDocker" == "true" ]; then
  sleep 10
fi

#RUN MIGRATIONS
./node_modules/db-migrate/bin/db-migrate up --force-exit

#RUN AND LOG OUTPUT
#npm install pm2@3.5.0 -g
#pm2 kill
#pm2 start ecosystem.config.js
#pm2 logs
node --inspect app.js
