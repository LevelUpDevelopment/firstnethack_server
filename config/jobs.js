/**
 * Default jobs configuration
 * (sails.config.jobs)
 *
 * For more information using agenda in your app, check out:
 * https://github.com/vbuzzano/sails-hook-jobs
 */
module.exports.jobs = {

  // Where are jobs files
  "jobsDirectory": "api/jobs",

  // agenda configuration.
  // for more details about configuration,
  // check https://github.com/rschmukler/agenda
  "db": {
    "address"    : `${(process.env.MONGO_HOST ? process.env.MONGO_HOST : 'localhost')}:${(process.env.MONGO_PORT ? process.env.MONGO_PORT : '27017')}/jobs`,
    "collection" : "agendaJobs"
  },
  "name": "process name",
  "processEvery": "10 seconds",
  "maxConcurrency": 20,
  "defaultConcurrency": 5,
  "defaultLockLifetime": 300000,
  "enviromentToRunIn": "JOB_ENV"
};